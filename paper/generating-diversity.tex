
\ifdefined\build\else\input{head}\fi
\section{Automated Generation of Diversity}\label{sec:clonal_plasticity} 
In this section, we introduce a self-adaptation mechanism called \textsl{clonal plasticity} which can be used by a group of agents to self-diversify. We use Clonal Plasticity, first introduced in \cite{Nallur2016Clonal} as an adaptation mechanism, to generate diversity automatically amongst the algorithms implemented. We show that this generated diversity is competitive with the manually introduced diversity presented in the previous section.

We had previously used clonal plasticity~\cite{Nallur2016Clonal} to enable decentralized adaptation, based on localized environmental changes. Due to its decentralized nature, the adaptations performed by multiple agents in the system, results in variations being naturally produced at a system-wide level. The process of clonal plasticity is briefly explained in the following sub-section.


\subsection{Clonal Plasticity Process}
Clonal Plasticity is an adaptation strategy, formed through the combination of two processes present in plants:~\emph{(a)} \textsl{clonal reproduction}, and \emph{(b)} \textsl{phenotypic plasticity}. \textsl{Clonal Reproduction} refers to "... sequential reiteration of a basic structural unit or module consisting of the leaf with its associated auxiliary bud and a segment of stem that connects it to other units"~\cite{Harper1980}. The fundamental idea in this form of reproduction is that the daughter plant is genetically identical to the parent plant, and hence the term \textsl{clone}. \textsl{Phenotypic Plasticity} refers to the ability of an organism to vary its structure (height of stem, width of leaves, etc.) or behaviour (connect to its clonal sibling or disconnect) according to environmental influences. Importantly, this adaptation does not include variation in an organism due to genetic factors. We combine these two mechanisms to propose a self-adaptation mechanism called \textsl{clonal plasticity}. Clonal Plasticity is distinguished from other evolutionary mechanisms such as \textsl{genetic algorithms}, in that the genome of the organism is \textbf{not} modified at all. From a software system's perspective, this means that a clonally plastic software will deviate in some aspects of its behaviour, but \textbf{will functionally remain the same}.

The process of plastic reproduction, depicted in Figure~\ref{fig:clonal-plasticity}, is given by the following steps:

\begin{enumerate}
  \item \textbf{Identify Plasticity Points}: At a given moment in time, \texttt{t}, of the system's lifetime, each individual in the system (depicted a circle in Figure~\ref{fig:clonal-plasticity}) identifies all its parts that can take multiple representations, values, or behaviour.
  
  \item \textbf{Evaluate Environmental Input}: Simultaneously, the fitness of each individual is self-evaluated in the following way: for each plastic point, environmental feedback is evaluated to check whether it impedes or favours that point. Feedback from the environment may come from different sensors or neighbouring individuals, or the individual's own reproduction memory (\texttt{m} in Figure~\ref{fig:clonal-plasticity}).

  \item \textbf{Plasticity Memory}: All individuals that have reproduced before, keep a record of the previous Plasticity Range chosen during the last reproduction cycle (variable \texttt{m} in Figure~\ref{fig:clonal-plasticity}). This allows an individual to repeat a good adaptation strategy, if it previously had a positive reward, or alternatively abandon a poor adaptation strategy.
 
  \item \textbf{Choose Plasticity Range}: During the following time-step (\texttt{t+1}), each individual chooses a plastic response, based on the evaluated environmental input. The available responses may be one of: 
  \begin{enumerate}%[label=(\arabic*)]
  \item Exact Clone, 
  \item Low Plasticity, and 
  \item High Plasticity.
  \end{enumerate}
	
  \item \textbf{Clone and Modify Plastic Points}: During the same time-step, each individual makes a clone of itself. The individual's plasticity points may be adapted according to the chosen strategy. The entire process starts again with a new time-step, \texttt{t}$'$, on all clones. 
\end{enumerate}

\begin{figure*}[tbhp]
  \centering
  \includegraphics[scale=0.65]{figures/clonal-plasticity/clonal-plasticity-process.pdf}
  \caption{Clonal plasticity process}
  \label{fig:clonal-plasticity}
\end{figure*} 

\begin{algorithm}[!h]
\DontPrintSemicolon
\SetAlgoLined
 \SetKwFunction{EvalEnviron}{EvalEnviron}
 \SetKwFunction{ChoosePlasticityRange}{ChoosePlasticityRange}
 \SetKwFunction{CreateClone}{CreateClone}
  \SetKwFunction{ModifyAgent}{ModifyAgent}
 \SetKwProg{myalg}{Algorithm}{}{}
 \KwData{PlasticSet, Memory, LastReward}
 \KwResult{AlgorithmClone}
\small
\myalg{\CreateClone{PlasticSet, Memory}}{
 \ForEach{$Agent\ a$}{
 	$LastReward_{a} \leftarrow EvalEnviron(a, PlasticSet_{a}) $\;
 	
	$WhichRange_{a} \leftarrow ChoosePlasticityRange(LastReward_{a}, Memory_{a})$\;

 		\Switch{$WhichRange_{a}$}{
 			\Case{Exact-Clone}{
 				clone $\leftarrow$ a\;
 				\Return clone\; 			
 			}
 			\Case{Low-Plastic-Clone}{
 				clone $\leftarrow$ a\;
				$plasticity-point, direction \leftarrow Memory_{a}$\;
				ModifyAgent($clone, plasticity-point, direction$)\;
 				\Return clone\;
 			}
 			\Case{High-Plastic-Clone} {
 				clone $\leftarrow$ a\;
				last-plasticity-point, last-direction $\leftarrow$ Memory$_{a}$\; 				
				plasticity-point $\leftarrow$ chooseDifferent(PlasticSet$_{a}$, last-plasticity-point)\;
				$direction \leftarrow chooseDifferent(last-direction)$\;
				ModifyAgent($clone, plasticity-point, direction$)\;				
 				\Return clone\;
 			} 
 		}
	}
}
\myalg{\ChoosePlasticityRange{LastReward, Memory}}{
	\eIf{$LastReward < 0$}{
		\Return Low-Plastic-Clone\;
	}{
	\eIf{$Memory > 0$}{
			\Return Exact-Clone\;
		}{
			\Return High-Plastic-Clone\;
		}
	}
}
\myalg{\EvalEnviron{$Agent\ a$}}{
		
}
\myalg{\ModifyAgent{$Agent\ a, plasticity-point, direction$}}{
		
}
\caption{Create a plastic clone of an algorithm \label{algo_plastic_change}}

\end{algorithm}

\subsubsection{Cloning Frequency:}
Every adaptation action, in any system, incurs a cost, either computational or communicative. It must be possible to evaluate the effect of the cloning process, before creating the next plastic clone. \texttt{t}$'$ refers to the next time-step when it is feasible to perform the next cloning process. In the agent that implements plasticity, the granularity of time when the next iteration of the cloning process is done, is domain dependent. For instance, domains such as traffic and smart-grids are different in how dynamically their respective environments change. To allow for each adaptation to take effect, we introduce the notion of \textit{cloning frequency}. Cloning Frequency refers to the time-period for which an agent is evaluated before the next iteration of the cloning process starts.

\subsubsection{Choosing the Plasticity Range:}
The process of choosing the plasticity range is the critical part in generating a diverse population. This process of modifying the plastic points considers both positive and negative feedback from the environment. For a given time instant \texttt{t}, the adaptation decision depends on two factors:

\begin{enumerate}
	\item Feedback from environment at time \texttt{t}, whether it is positive or negative.
	\item Memory of the modification action, taken at time \texttt{t-1}.
\end{enumerate}

If the feedback from the environment is positive, and memory of the previous 
action is also positive, then the Plasticity Range is chosen to be \emph{Exact Clone}. If the feedback from the environment is positive, but the memory of the 
previous action is negative, then the Plasticity Range is chosen to be \emph{Low 
Plasticity}. If the feedback from the environment is negative, then the Plasticity Range is always \emph{High Plasticity}
    
\subsubsection{Exact Clone}
In this case, the individual simply makes a copy of itself, with no change at all.

\subsubsection{Low Plasticity}
Modification with Low Plasticity implies that the individual chooses the same plasticity point, as chosen during the previous cloning process, and moves in the same direction as before.

\subsubsection{High Plasticity}
Modification with High Plasticity implies that the individual chooses a different plasticity point, than was chosen during the previous cloning process, and moves in a random direction.

\subsection{Clonal Plasticity for MG}
Here, we modify the agents, such that each agent is able to reproduce in a clonally plastic way. Cloning, as opposed to other evolution-based mechanisms, is completely decentralized as there is no universal fitness function that sorts the population in to \textit{fit} and \textit{unfit} individuals. 

Making an algorithm plastic requires following the process of plastic reproduction
\begin{enumerate}
	\item \textbf{Identification of Plasticity Points}: BestPlay has only one variable that influences its decision-making, \ie \texttt{memory}. Hence, there is only one plasticity point for agents implementing BestPlay. Similarly, for the Evolutionary strategy, there is one variable that influences how the decision making varies within the population, \ie \texttt{mutation}. Roth-Erev has two variables that can be plasticized, \ie \texttt{recency} and \texttt{epsilon}
	
	
	\item \textbf{Evaluate Environmental Input}: The environment allows each agent to know how well its algorithm is doing. That is, depending on the performance of the agent in successfully choosing the minority group, the agent would have either get positive or negative feedback.  In the context of MG, cloning frequency refers to the number of times an agent plays the game, before its performance is evaluated. Thus, a cloning frequency of $5$ means that an agent's performance is evaluated after every $5$ rounds. Clearly, the lower this number, the faster the agent reacts to the environmental feedback. Agents playing BestPlay or Roth-Erev were evaluated with cloning frequency ranging from $5$ to $105$ rounds. Agents playing the Evolutionary strategy were evaluated with cloning frequency ranging from $20$ to $100$. This difference is due to the fact that preliminary experimentation revealed that the Evolutionary algorithm required $20$ rounds to produce a noticeable effect in an agent's performance. The effect of cloning frequency on diversity generation is discussed in section~\ref{sec:eval-clonal-plasticity-in-diversity-generation}. The memory ($m$) of the previous algorithm parameters allows an agent to return to a previously known good configuration, if the current modification resulted in losses.
	
	\item \textbf{Choose Plasticity Range}: The plasticity range chosen by a particular agent defines how the agent decides to adapt. Each of the algorithms (BestPlay, Evolutionary, Roth-Erev), depending on its choice of parameters, decides how the agent would play upcoming rounds of MG. Any change of these parameters could cause the agent to change its decision, and thereby affect the rewards that it accumulates. 

\end{enumerate}
%
%\subsubsection{Clonal Plasticity for GA}
%\begin{enumerate}
%	\item Identify Plasticity Points for Roth-Erev
%	\item Evaluate Environmental Input
%	\item Choose Plasticity Range
%\end{enumerate}
%\subsubsection{Clonal Plasticity for Reinforcement Learning}
%\begin{enumerate}
%	\item Identify Plasticity Points for Roth-Erev
%	\item Evaluate Environmental Input
%	\item Choose Plasticity Range
%\end{enumerate}

%Using the plastic reproduction process, we change the strategy of each agent in the following way:
%\begin{enumerate}
%	\item \textbf{Identify Plasticity Points}: For the minority game, there are two plasticity points, \textit{i.e.} strategy length and number of strategies.
%	
%	\item \textbf{Evaluate Environmental Input}: The environmental input is the individual's success during the reproduction cycle.
%	
%	\item \textbf{Choose Plasticity Range}: If the individual agent has won (been on the side of the minority) more than half the rounds during its reproduction cycle, it chooses \emph{Exact Clone}. If the agent has won fewer than half, but more than quarter of the rounds during its reproduction cycle, it chooses \emph{Low Plasticity}. If the individual has won fewer than a quarter of the rounds in its reproduction cycle, it chooses \emph{High Plasticity}. In nature, a individual that is fitter is more likely to survive than a weaker one. This allows plastic individuals, on average, to propagate more than a-plastic individuals. Since the minority game is inherently a competitive game, we allow strong individuals to take over a weak individual, with a small probability. `Taking over' is modelled by cloning the fit neighbour's parameters onto the weak individual.
%	
%	\item \textbf{Plasticity Memory}: Based on the previous plasticity range chosen, and the current plasticity range, an individual chooses if it wants to move in the same direction as its previous plastic response, and by what magnitude. Thus, an individual agent could either change his memory-length, or change the number of strategies in its pool.
%	
%	\item \textbf{Clone and Modify Plastic Points}: Each agent modifies its strategies according to its memory and current plasticity range.
%
%\end{enumerate} 




\subsection{Worked Out Example}
Now we perform the clonal plasticity process step-by-step for BestPlay. For details on the clonal plasticity process for the Evolutionary and RothErev algorithms, see appendices~\ref{sec:appendix-cp-for-evo} and \ref{sec:appendix-cp-for-re} (the process differs only in the plasticity points identified for each algorithm).

\begin{enumerate}
	\item \textbf{Identify Plasticity Points}: The BestPlay algorithm critically depends on the size of the memory, that it keeps of the game (see Table~\ref{mg_sample_strategies}). Two agents that diversify into different directions (one increasing the memory size, and the other decreasing) could end up with drastically different rewards. Here, $m$ is identified as a plasticity point of the algorithm. Another plasticity point is the number of strategies ($k$) that the agent possesses. 
	
	\item \textbf{Evaluate Environmental Input}: For any given time instant, the algorithm has a $50$\% chance of picking the right group (\textit{i.e.}, being in the minority). Hence, for any given period of cloning frequency, if the agent has accumulated (\texttt{wins-this-cycle}) more than $50$\% of the rewards during the period, the feedback is said to be positive. If the agent accumulates less than $50$\% of the rewards, then the feedback is negative. 
	
	\item \textbf{Plasticity Memory}: The agent keeps track of the rewards that it had accumulated during the previous adaptation (\texttt{wins-last-cycle}). Along with the feedback, this helps it decide what plasticity range to choose next (see Table~\ref{tbl:plasticity-range-criteria}). 
	
	\item \textbf{Clone and Modify Plastic Points}: In the event of \emph{Exact Clone}, the agent does not change any of its parameters. In the event of \emph{Low Plasticity}, the agent picks one of its plastic points and modifies it in the same direction, as the plasticity memory from the previous generation. In the event of \emph{High Plasticity}, the agent modifies \textit{all} its plasticity points to return to the values that it had from the previous generation.
	
\end{enumerate}

\begin{table}[]
\def\arraystretch{1.5}
\begin{tabular}{|l|l|l|}
\hline
\textbf{Feedback} & \textbf{Plasticity Memory}                   & \textbf{\begin{tabular}[c]{@{}l@{}}Plasticity Range\\ Chosen\end{tabular}} \\ \hline
Negative          & (Disregarded)                                & High Plasticity                                                             \\ \hline
Positive          & wins-this-cycle \textless= wins-last-cycle   & Low Plasticity                                                                \\ \hline
Positive          & wins-this-cycle \textgreater ~ wins-last-cycle & Exact Clone                                                                \\ \hline

	\end{tabular}
	\caption{Criteria for choosing plasticity range}
	\label{tbl:plasticity-range-criteria}
	\end{table}


\subsection{Experimental Setup and Results}
Table~\ref{new_experimental_constants} shows the constant factors in each experiment\footnote{All code for this experiment can be found at:\\ \url{https://bitbucket.org/viveknallur/algo_diversity_tse.git}}. Each minority game was played with a population size of $501$ agents, through a simulation time period of $2000$ steps. For each variation in the experimental setup, the data is reported as an average of 100 simulations. We measure the efficiency of the system through the median amount of rewards accumulated by the agent, after the simulation period. We measure the fairness of the system using the Gini index. The Gini index is used to calculate dispersion in the income distribution of a society~\cite{Gini1936Measure}. In a society that is perfectly equal, the Gini index is equal to zero. Therefore, the closer the Gini index is to zero, the fairer the distribution of rewards.

\begin{table}[ht!]
\centering
%\large
\def\arraystretch{1.5}
\begin{tabular}{|l|c|}
%\toprule
\hline
{\bf Variable}       & {\bf Value} \\ \hline
Population Size      & 501         \\ \hline
Simulation Period (rounds)    & 2000        \\ \hline
Repetition of Variation		   & 100  \\ \hline

\end{tabular}
\caption{Experimental constants - Same as manual process of diversification}
\label{new_experimental_constants}
\end{table}

Figures~\ref{fig:div-vs-reward-by-unified-algo} and \ref{fig:div-vs-gini-by-unified-algo} show the results of diversification introduced by clonal plasticity. As is immediately obvious from the x-axis (that shows diversity generated), manual diversification is able to generate a higher level of diversity in the populations of agents. Clonal Plasticity is able to reach a diversity level (also called H-index) of upto $2.7$, whereas manual diversification (see Figure~\ref{all_together}) is able to reach an H-index of upto $5.8$. This is due to the fact that a human (knowing the functional pathways of a particular algorithm) is able to specify diverse individuals that will generate diverse outputs. However, in terms of efficiency and equity, clonal plasticity confirms the results found by \textit{Nallur et al}.~\cite{Nallur2016Algorithm}. Note that as the diversity goes up, the median reward accummulated by the population also increases (Figure~\ref{fig:div-vs-reward-by-unified-algo}). 
At the lower end of the diversity scale ($H = 1.7$), the median reward is $509$, while at the higher end of the scale ($H = 2.7$), the median reward is $867$. Increasing the diversity of the population increases the reward achieved by each agent. Figure~\ref{fig:div-vs-gini-by-unified-algo} shows the effect on equity of rewards achieved. The \textit{y-axis} shows the Gini index, which is a quantitative measure of equality of reward. In Figure~\ref{fig:div-vs-gini-by-unified-algo}, the increase in diversity results in a decrease in the Gini index. 

\begin{figure}[!tbhp]
  \centering
%  \includegraphics[scale=0.55]{figures/clonal-plasticity/diversity-vs-reward-by-unified-algo-barplot.pdf}
  \caption{Impact of diversity on Reward}
  \label{fig:div-vs-reward-by-unified-algo}
\end{figure} 

\begin{figure}[!tbhp]
  \centering
 % \includegraphics[scale=0.55]{figures/clonal-plasticity/diversity-vs-gini-by-unified-algo-barplot.pdf}
  \caption{Impact of diversity on Equity}
  \label{fig:div-vs-gini-by-unified-algo}
\end{figure} 

\begin{table}
%\large
\centering
\def\arraystretch{1.5}
\begin{tabular}{|c|c|c|}
\hline
\textbf{Diversity }&  \textbf{Reward} &  \textbf{Gini} \\
\hline
1.7       &            509 &  11.2 \\ \hline
2.1       &            616 &   1.0 \\ \hline
2.2       &            755 &   1.3 \\ \hline
2.5       &            789 &   0.9 \\ \hline
2.6       &            770 &   1.1 \\ \hline
2.7       &            867 &   2.1 \\
\hline
\end{tabular}
\caption{Relationship between Diversity and Reward and Gini}
\label{tbl:diversity-vs-reward-gini}
\end{table}

\section{Effectiveness of Diversity in Fairness and Efficiency}
Recall from Section~\ref{sec:formal_disb_justice}, the theoretical optimum efficiency that we would like is $T/2$. That is, the accummulated reward at the end of the simulation should be equal to half the number of rounds of simulation. In all our experiments, the $T$ is kept as a constant $2000$, hence the optimum efficiency is $1000$. We see from Figure~\ref{fig:div-vs-reward-by-unified-algo}, that increasing diversity results in a increased median reward. At the highest diversity level (H = $2.7$), the accumulated reward is $867$, while at the lowest level of (H = $1.7$), it is $509$. 

Note also from Table~\ref{tbl:diversity-vs-reward-gini}, the lowest level of diversity is also responsible for the highest level of the Gini index. Recall that the Gini index measures the inequality of the distribution of income. Thus, the closer the Gini index value is to zero, the more equitable the distribution is. This table shows that attaining both, efficiency of the mechanism (close to the theoretical optimum) and fairness (a theoretical optimum of zero) is a difficult problem. While increasing diversity directly increases the efficiency ($867$ is closer to $1000$ than $509$), it does not have the same clear impact on fairness. While the fairness achieved with the highest diversity (Gini index of $2.7$) is certainly better than the fairness achieved with low diversity (Gini index of $11.2$), it is interesting to see that a diversity level of $2.5$ actually provides the highest amount of fairness (Gini index of $0.9$). This indicates the system might have an optimal `sweet spot' for diversity, and going beyond such a limit might not necessarily improve both efficiency and fairness. Automatically deriving the pareto curve of diversity, for a particular system, would be an interesting avenue of further research.


%\FloatBarrier
\subsection{Evaluating Clonal Plasticity in Generation of Diversity}\label{sec:eval-clonal-plasticity-in-diversity-generation}

We now look at Clonal Plasticity itself and evaluate how the cloning frequency influences the amount of diversity achieved within the population. Cloning frequency refers to how many rounds (of time) elapse before the cloning process is run.  Intuitively, the fewer the number of rounds between each cloning process, the higher the amount of diversity that will be achieved. This is due to the fact that each run of the cloning process has the potential to create a new specie. This is borne out by the data in Table~\ref{tbl:div-vs-numspecies} which illustrates that an increase in mean diversity has a direct correspondence with an increase in the mean number of species generated by the cloning process. Figure~\ref{fig:div-clonFreq-numSpecies} shows a population-wide distribution of the same analysis

\begin{table}
\centering
\def\arraystretch{1.5}
\begin{tabular}{|l|r|r|}
\hline
\textbf{{Algorithm}} &  \textbf{Diversity} &  \textbf{Num\_Species} \\
\hline
BestPlay              &               1.7 &                  60 \\ \hline
Evo                   &               2.7 &                 501 \\ \hline
RothErev              &               2.7 &                 486 \\ \hline
BestPlay-Evo          &               2.1 &                 271 \\ \hline
RothErev-BestPlay     &               2.2 &                 303 \\ \hline
RothErev-Evo          &               2.7 &                 493 \\ \hline
Evo-RothErev-BestPlay &               2.5 &                 419 \\ \hline
\end{tabular}
\caption{Diversity and Number of Species}
\label{tbl:div-vs-numspecies}
\end{table}

\begin{figure}
\includegraphics[scale=0.47,clip=y, trim=8mm 0 0 0 0]{figures/clonal-plasticity/div-clonFreq-numSpecies-all-algo.png}
\caption{Diversity, cloning frequency and number of species together}
\label{fig:div-clonFreq-numSpecies}
\end{figure}

In Figure~\ref{fig:div-clonFreq-numSpecies}, we see the distribution of diversity across the entire population as bubbles (\textit{y-axis}), with change in cloning frequency(\textit{x-axis}). The size of the bubbles represents the number of species. As we go up the \textit{y-axis}, notice that the size of the bubbles increases. This indicates that the greater the diversity, the greater the number of species generated. Also, the lower the number of rounds between each cloning process (\ie, the lower the frequency), the higher the mean amount of diversity produced. Table~\ref{tbl:full_table} in Appendix~\ref{sec:appendix-b} shows the full breakdown of the number of species produced, as the cloning frequency varies, per algorithm. It is interesting to note that the cloning frequency does not, by and large, affect the diversity achieved \textit{within} an algorithmic group.

\subsection{Effect of Plasticity on Different Algorithms}
Figures~\ref{fig:algo-wise-reward} and \ref{fig:algo-wise-gini} show an algorithm-wise breakup of differences in reward achieved as well as the Gini index. Read in conjunction with Figure~\ref{fig:algo-wise-num-species}, which shows that three algorithms (\texttt{Evo, RothErev} and \texttt{RothErev-Evo}) achieved the highest diversity, it also shows that high levels of diversity are linked to high levels of reward. Table~\ref{tbl:full_table}, from Appendix~\ref{sec:appendix-b} shows how this can be further broken down based on how rapidly the cloning process operates.

\begin{figure}
\centering
\includegraphics[scale=0.45,clip,trim={0 0.76cm 0 0.76cm}]{figures/clonal-plasticity/reward-vs-algorithm.png}
\caption{Algorithm-wise breakup of Reward achieved}
\label{fig:algo-wise-reward}
\end{figure}


\begin{figure}
\centering
\includegraphics[scale=0.45,clip,trim={0 0.76cm 0 0.76cm}]{figures/clonal-plasticity/gini-vs-algorithm.png}
\caption{Algorithm-wise breakup of Gini Index}
\label{fig:algo-wise-gini}
\end{figure}

Consider Figure~\ref{fig:algo-wise-reward} and \ref{fig:algo-wise-gini} along with Table~\ref{tbl:diversity-vs-reward-gini} and \ref{tbl:div-vs-numspecies}. Both \texttt{BestPlay-Evo} and \texttt{RothErev-BestPlay} achieve a mean Gini-index very close to zero($1.0$ and $1.3$), however it is the combination of \texttt{Evo-RothErev-BestPlay} that achieves the best Gini-index ($0.9$) and as well as a very high level of mean reward ($780$). \texttt{Evo-RothErev-BestPlay} performs very well across both indicators of reward and Gini. However, if one considers only rewards, it is \texttt{RothErev} with the highest diversity level of $2.7$, that has the highest reward level. In contrast, \texttt{Evo} is another algorithm that reached a high level of diversity; almost all its individuals consistently formed separate species (see Figure~\ref{fig:algo-wise-num-species}) and a mean Gini-index very close to zero. However, an examination of Figure~\ref{fig:algo-wise-gini} shows that the distribution of Gini values fluctuated quite a bit (see Table~\ref{tbl:full_table} from Appendix~\ref{sec:appendix-b} for a cloning-frequency-wise breakdown), indicating that it is sensitive to cloning-frequency values. Taken together, the results show that raising diversity levels indiscriminately is not a silver bullet for achieving fairness, however reward levels rise in congruence with diversity.


\begin{figure}
\centering
\includegraphics[scale=0.45,clip,trim={0 0.76cm 0 0.76cm}]{figures/clonal-plasticity/numSpecies-vs-algorithm.png}
\caption{Algorithm-wise breakup of number of species at the end of cloning}
\label{fig:algo-wise-num-species}
\end{figure}



%\FloatBarrier

\section{Generalizing Clonal Plasticity for Other Systems}\label{sec:eval-clonal-plasticity} 
The first step in generating a diverse population through clonal plasticity, is the identification of plasticity points. In Figure~\ref{fig:identify_plasticity_points}, we show a general flowchart that aids an engineer in identifying relevant plasticity points.


\begin{figure}
\centering
\includegraphics[scale=0.35, clip=true, trim=0 0 3 0]{figures/clonal-plasticity/Identification-of-plasticity-points.pdf}
\caption{Identification of Plasticity Points}\label{fig:identify_plasticity_points}
\end{figure}


\subsection{Steps for Creating Plastic Clones}
\begin{enumerate}
	\item Identify the set of \emph{different} algorithms
	\item For each algorithm: identify all plastic parameters
	\item For each plastic parameter: identify the range of possible values it can take
	\item Each combination of plastic values that results in a different output is a specie
	\item Generate individuals for each specie with different proportion in population \label{generate_individuals}
	\item Measure diversity of population\label{measure_population_diversity}
	\item Repeat step~\ref{generate_individuals} and \ref{measure_population_diversity} for all feasible proportions
\end{enumerate}

\subsection{Scalability of Clonal Plasticity}
%
%Calculate complexity value of main algorithm (createClone). Reflect on how `unexplained' algorithms affect other systems.
Although the clonal plasticity process is effective at generating diversity, its scalability is an important factor in deciding whether it is feasible for implementation in real systems.

Before the cloning process can begin, the identification of the plasticity points must be done~(Figure~\ref{fig:identify_plasticity_points}). This is a one-time function and needs to be done only at the initialization of the plasticity process.
For any algorithm \texttt{a}, with \texttt{k} interface variables, the first step is to identify whether those variables modify the output of the algorithm. This is clearly $O(k)$ in complexity. The second major step is to identify the plasticity range for each plasticity point. This involves identifying the valid set of values a plasticity point may take, as well as the constraints on its value. Assuming a time complexity of $r$ per variable, the first phase of clonal plasticity is $O(r*k)$ in complexity.

Recall that the \texttt{ChoosePlasticityRange} and \texttt{CreateClone} functions form the core of the plasticity process (see Algorithm~\ref{algo_plastic_change}). \texttt{ChoosePlasticityRange} is a linear function ($O(c)$) since it involves  merely decision-making, based on the \texttt{LastReward} and \texttt{Memory} variables. \texttt{CreateClone} involves looking at the value of each plasticity point, and assuming a time complexity of $p$ per plasticity point, its complexity can be calculated as $O(p*k)$. The computational complexity of the clonal plasticity process is therefore: $O(r*k) + O(c) + O(p*k)$. This is important because, if the adaptation process is computationally expensive, then its benefits are limited to  systems with few plasticity points. The linear complexity of clonal plasticity implies that it can be used for systems comprising of agents with a high number of plasticity points.

The \texttt{EvalEnviron} is a domain-specific function to evaluate the current performance of the agent, and is therefore left unspecified. Any adaptation mechanism would require an \texttt{EvalEnviron} function and as such, it is a price that all adaptation functions must pay for sensible self-modification. 


Crucially, it is important to note that the entire process works on a single agent and is independent of other agents' performance, in any direct fashion. This allows the plasticity mechanism to work in a completely decentralized manner. Of course, the presence of other agents affects the evaluation of the environment (in an indirect fashion via their actions), and thus provides local competitive pressure that moves the agent to a better configuration. 

\ifdefined\build\else\input{tail}\fi
