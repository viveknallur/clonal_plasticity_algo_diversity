#Algoritmic diversity

- **Venue:** [IEEE TSE](http://www.computer.org/web/tse) 

## Description
This repo contains the latex structure for the paper describing algorithmic diversity. 
The idea of the paper is to present algorithmic diversity by providing a first definition of what that means, and experiments demosntraiting why it is a good idea to have algorithmic diversity. 

The main file of the paper is _main.tex_ containing the structure definition and references to all input files. There is a file for each independent section of the paper.


## Structure 

* preamble: contains all package and command definitions. Everything should be set by now, but if something needs defining this is the place to do it (look inside to see the structure)
* bibfiles
    * local.bib: contains all local bib references i.e., references used for this paper specifically. Most references should be added here
    * bib folder: this folder contains many references used for multiple purposes, in particular the _compsci.bib_ file contains many references on software engineering, programming languages, and adaptive systems. Check here if the reference exists before adding it to the _local.bib_ file
* Acronym.tex: contains all Acronyms used in the paper (look into the file for example of the definition). Acronyms are used with the \ac{ACRONYM} command


## Useful commands

* _\comment[TYPE]{AUTHOR}{COMMENT}_ is used to leave annotation inlined with the text. Comments only appear in draft mode (the option in the documentclass in the _main.tex_ file) and are invisible otherwise. Type can be comment, missing, idea, note, and author.
* _\ie \eg \cf_ are used respectively for the abbreviations i.e., e.g., cf.
* listings are defined according to each specific language in the _preamble_ file and generate their own environment (e.g., _\begin{algo}[....]{....}_ generates a generic listing).