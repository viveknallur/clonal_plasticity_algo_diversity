% !TEX root = tse.tex
\ifdefined\build\else\input{head}\fi

%%
\section{Algorithmic Diversity}
\label{sec:define_diversity}
Decision-making and resource allocation mechanisms are all realized by various algorithms, and hence we view algorithmic diversity as an appropriate level of granularity for achieving our goals. We define \textsl{Algorithmic Diversity} as: a variability in the output produced by a set of algorithms, when faced with the same input/input-sequence. We do not consider differences in the internal data structures used (stacks, queues, trees, etc.) or control flow implemented (iteration, recursion, continuations, labelled jumps, etc.) for two reasons:
\begin{enumerate}
	\item Detecting whether two algorithms are the same even when they are exceedingly simple in their construction is so hard that for practical purposes it is infeasible~\cite{Blass2008When}
	\item More philosophically, from the perspective of the environment (or other agents), if two systems produce exactly the same output (behaviour), regardless of the input sequence, then they are identical, regardless of their internal construction.
\end{enumerate}

This definition positions us in domains where there are multiple valid answers and no deterministic ways of calculating the optimal response in advance. Autonomous systems fall squarely into this category since the environment in which they operate is dynamic in nature. Traffic flow management systems are a good example of dynamically changing environments. Each vehicle has (possibly) different source and destinations and different strategies in how it gets there (\eg fastest route, shortest route, least polluted route, etc.). 
%Managing route-choices and vehicle speeds in a centralized manner has not yielded very good results. In fact, Nagel~\cite{Nagel1996} stresses that ``neither a driver nor an omniscient traffic management system can decide which of several possible paths might be best. In this way, it is the increasing unpredictability \textit{caused by the centralized traffic management} which eventually impedes further improvement." Therefore, we move away from the centralized approach and evaluate a decentralized approach to traffic simulation and management. This decentralized method of traffic simulation has been characterized as a self-organizing game~\cite{Nagel1996}, where the aggregate result of congestion or smooth flow is dependent on the individual action of each driver. In such decentralized traffic management mechanisms, the natural model is to create agents representing vehicles and allow them to compete for allocation of road-sections, based on their individual strategies. 
It is in these scenarios that aggregate outcomes such as efficiency and fairness are important. While autonomous agents may negotiate amongst themselves to reach an efficient allocation of road resources, it is not necessary that this allocation is fair. An agent (or a set of agents) with a superior algorithm may dominate the population of agents, and grab resources at every opportunity. It is in this context that we hypothesize that diversification of algorithms leads to a fairer distribution of rewards amongst the population, regardless of the actual algorithms in use. This hypothesis was first formulated and tested in~\cite{Nallur2016Algorithm}. In this work, we extend the results as well as introduce an autonomic mechanism to allow agents to self-diversify. We also compare the effectiveness of the autonomic mechanism against the manual efforts earlier.

We use a self-organizing game called the Minority Game (MG)~\cite{Challet1997} as our exemplar Multi-Agent System in a traffic setting, primarily because it has been well-studied and well-understood. The Minority Game (MG), introduced by Challet and Zhang, consists of an odd number (N) of agents, playing a game in an iterated fashion\footnote{Note that the exact number of agents does not impact the results, as long as there is a well-defined minority at every stage.}. At each timestep, each agent chooses from one of two actions (A or B), and the group that is in the minority, wins. Since $N$ is an odd integer, there is guaranteed to be a minority. Winners receive a reward, while losers receive nothing. After each round, all agents are told which action was in the minority. This feedback loop induces the agents to re-evaluate their action for the next iteration. 

%\subsection{Measuring the diversity of a family of algorithms}
%\label{sec:algorithm}
%
%The observable behaviour of software systems is encompassed by a set of functions, ---that is, its 
%algorithms. Such algorithms are specified by giving its inputs and expected outputs exhibiting, as a 
%consequence, variations of their behaviour depending the concrete implementation technique used. 
%These variations arise from the different possible implementations for a particular algorithm, 
%yielding similar functional behaviour with respect to the specification, yet 
%exhibiting different quality properties in the overall execution of the system. Differences in the 
%programming paradigm, optimization techniques used, or the type of algorithm implemented (\eg 
%recursive vs. iterative) could have an impact on the behaviour of the system. Having identified these 
%differences, we observe that the behaviour of a software system can be satisfied by a 
%\emph{variety} of algorithms rather than by a single function. 
%
%we explore the effect of algorithmic diversity in a software system ---that is, 
%the effect of using multiples varieties of algorithms to satisfy a goal, during the execution of the system. 
%This exploration is based on the following research question. \emph{``Does using a diverse family of 
%algorithms in a system have an effect on its overall observable behaviour?''} The notion of diversity used 
%for this exploration is anchored on the ecological model for classification of individuals in an ecosystem. 
%That is, defining the diversity of a system in terms of the species \emph{varieties} present in the system, 
%\emph{balance} in which such species are represented, and the \emph{disparity} between the varieties 
%present in the system. 
%In the particular case of algorithmic diversity we are interested in identifying the number of 
%\emph{varieties} for a functional specification (\ie how many different concrete algorithm implementations can be defined to satisfy a specification), and how many of those varieties should be present in the system (\ie what is the \emph{balance} of the system). With these two concepts defined in the case of algorithms, we can tackle the algorithmic diversity question by dividing the work in two research tracks: 
%
%\begin{enumerate}
%  \item How to measure the diversity between a family of algorithms. This track focuses in providing an 
%  automated process to measure whether two concrete algorithm implementations belong to two different 
%  varieties, or they are two representative individuals of the same variety.
%  \item How to measure the effect of applying algorithmic diversity to a system. This track focuses on identifying the amount varieties that should be present in a system to maximize its quality properties.
%\end{enumerate}
%
%In order to define an automated metric to characterize varieties in a family of algorithms, it is, alas, not possible to reuse metrics as the Shannon index, applied to ecosystems. This is because, in algorithms there is not a clear notion of what a species is, since concrete implementations of different algorithms may be clustered in different ways. Using the Shannon index, would require a human expert to define the species and individuals for each species, breaking the requirement of measuring algorithmic diversity in an automated process. Take for example the domain of sorting algorithms, where given a list of elements, the result of the algorithms is a second list in which all the elements are organized in and ascending order with respect to some ordering function. In this example, algorithm species could be manually created according to the programming language used for the implementation (\eg Python, or Java), the specific type of the algorithm (\eg bubble-sort, or quick-sort), the implementation technique used (\eg iterative, or recursive), or the complexity class of the algorithm (\eg logarithmic, or quadratic). 
%
%In order to unequivocally define the clustering of concrete algorithm implementations to define the varieties in a system  we use a distance function that given two concrete algorithm implementations, returns how far apart they are from each other. In order to talk about diversity at the algorithmic level, the distance function and algorithms should satisfy the following properties.
%\begin{enumerate}
%  \item The distance should be normalized and bounded for all types of algorithms, so that they are comparable with each other.
%  \item The algorithms used should be such that given the same input, are able to produce different outputs.
%\end{enumerate}
%
%%CONCLUSION
%Taking into account these observations, the unified definition of algorithm diversity is as follows
%
%\begin{description}
%   \item[Variety:] Given a family of algorithms $F$, a variety is defined as the sets of algorithms producing different outputs to a same given input. Given a binary distance function $d$, a variety is characterized by the equivalence class on $F$ from $d$ --- that is, $F/d$, where for an algorithm 
%   \[
%   x, [x]_d = \{ y \in F \, | \, d(x,y) \rightarrow 0 \} 
%	\]   
%   Conversely, if $d(x, y) \rightarrow 1$,  we say that the algorithms belong to different varieties. The number of varieties in a system is given by the number of equivalence classes $|F/d|$.
%   \item[Balance:] The balance of the system is given by the proportion of each algorithm variety in the population of algorithms that are used concurrently in the system. For a particular algorithm $x$ the balance of the variety it generates, $w_x$ is: 
%   \[ w_x = |[ x ]_d| \]
%   \item[Disparity:] The disparity of the system is defined by a function expressing the overall variety in the system. Our binary distance function is symmetric (\ie $d(x,y) = d(y,x)$) by construction, and hence the overall variety in the system can be expressed as a \emph{symmetric, hollow matrix}. This leads to a definition of a disparity as the sum of the elements in the upper / lower triangle of the variety matrix. 
%   \[ disparity = \frac{1}{2} \sum_{x,y \in F} d(x,y) \]
%   For example, if the variety matrix for five algorithms ($a, b, c, d, e$) was given as $V$ (as shown), then the disparity would be the sum of upper / lower triangle of $V$.
%   \[
%   V = 
%\begin{blockarray}{cccccc}
% & a & b & c & d & e \\
%\begin{block}{c(ccccc)}
%  a & 0 & 0.7 & 0.5 & 0.1 & 0.9 \\
%  b & 0.7 & 0 & 0.4 & 0.3 & 0.3  \\
%  c & 0.5 & 0.4 & 0 & 0.4 & 0.6  \\
%  d & 0.1 & 0.3 & 0.4 & 0 & 0.7  \\
%  e & 0.9 & 0.3 & 0.6 & 0.7 & 0  \\
%\end{block}
%\end{blockarray}
% \]
% 
% \item [Diversity:] Diversity can now be computed as an average of the total disparity, weighted by the balance of the system.
% \[ \frac{1}{2} \sum_{x,y \in F} \frac{d(x,y)}{w_x}\]
%\end{description}
%
%
%
%\subsection{Defining Algorithmic Diversity for Load Balancing}
%\textsl{Load Balancing} refers to the function of distributing incoming web-requests amongst multiple servers, such that no single server is over-loaded and some performance metric (e.g. number of users served) is maximized. Load balancing is a fairly common infrastructural function that is used by most websites, and is key to the smooth functioning of a website. In the ideal case, incoming requests are distributed amongst the servers that serve the actual content (called \textsl{content servers}) in such a manner that all requests are satisfied. If the number of incoming requests is too large, the load-balancer is overwhelmed, and starts to drop requests. Increasing the robustness of the load-balancer therefore, would mean minimizing the number of dropped requests in the face of overwhelming traffic.
%
%\paragraph{}
%
%The pseudocode in Algorithm~\ref{alg:create_algorithmic_diversity} shows the steps involved in the process of creating a pool.
%
%\IncMargin{1em}
%\begin{algorithm}
% \SetAlgoLined
% \DontPrintSemicolon
% \KwData{$AlgoSet \longleftarrow $Set of functionally similar algorithms naturally available(Fig~\ref{fig:original_system})}
% \KwResult{$PoolSet \longleftarrow $Set of pools to choose from(Fig~\ref{fig:diverse_system})}
% $PoolSet \leftarrow \emptyset$\;
%
%  \For{pool\_size $\leftarrow 1$ \KwTo $NumAlgorithms$}{
%  new\_pool $\leftarrow []$\;
%  \For{$i\leftarrow 1$ \KwTo $pool\_size$}{
%  	$AppendToPool(new\_pool) \leftarrow ChooseWithoutReplacement(AlgoSet)$\;	
%  }
%  $PoolSet \leftarrow new\_pool$\;
%  }
% 
% return $PoolSet$\;
% \caption{Creating Algorithmic Diversity}
% \label{alg:create_algorithmic_diversity}
%\end{algorithm}
%
%\begin{figure}[t]
%	\includegraphics[scale=0.85,clip=true, trim=7cm 16cm 0 3cm]{figures/load_balancing/original_system.pdf}
%	\caption{Original system: One algorithm is chosen}
%	\label{fig:original_system}
%\end{figure}
%
%\begin{figure}[t]
%	\includegraphics[scale=0.85,clip=true, trim=8cm 15cm 14cm 0]{figures/load_balancing/diverse_system.pdf}
%	\caption{Diverse system: A pool is chosen}
%	\label{fig:diverse_system}
%\end{figure}
%
%Given a particular pool, the load-balancer chooses an algorithm randomly from this pool. This choice is maintained for a period of time, and then on the receipt of a signal, the balancer switches algorithms from within the pool. The load-balancer thus rotates amongst the various algorithms in the pool. This simple concept can have a dramatic impact on the robustness of a system, and we show (in the next section) that adding diversity in this manner, can decrease failure rates.





%From our definition of algorithmic diversity, it is easy to envisage a system that diversifies its output, based on environmental cues, by switching between multiple algorithms. However, a more interesting case is when for the same input environment, different parts of the system use different algorithms. A \emph{multi-agent system} (MAS) is an example of such a system, where multiple agents can potentially use different algorithms to achieve the same goals, for \emph{exactly} the same input environment. This allows us to clearly identify the gains made due to diversification, as opposed to some other modification or environmental change.


%A diversity index is a quantitative measure of the number of different types and the abundance of those types, in a given dataset. Intuitively, the higher the diversity index, the higher the diversity of the dataset. 
%However, there are no mechanisms for differentiating amongst algorithms, that can be commonly applied across multiple domains, apart from the \emph{Big-O} complexity calculation. The \emph{Big-O} notation does not, however, yield useful information about the \emph{different-ness} of algorithms. There may be two algorithms that have the same \emph{Big-O class}, but are completely different in terms of data-structures. For instance, Quicksort and Heapsort are very different algorithms, but their average case time complexity is the same, $O(n log(n))$! It also does not help us differentiate between two algorithms that use the same data-structures but return \emph{different outputs}. Our interest in algorithm diversity is due to the potential differences in the functional pathways, and consequently the outputs, for the same given input-sequence.  This is so because it is this diversity of outputs that we wish to exploit, as a strategy to deal with dynamic environments. In such cases, the Big-O calculation does not yield any useful insights. %Hence we look to Ecology, where the notion of diversity has been discussed extensively~\cite{Tuomisto2010}. 

%% Reference the results of the Normalized Compression Distance for algorithms


%\subsection{Measuring Algorithm Diversity for Routing Application}
%Talk about routing algorithms and the \textbf{lack of diversity} for SmartHopper

\ifdefined\build\else\input{tail}\fi