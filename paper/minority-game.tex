
\ifdefined\build\else\input{head}\fi
\section{Minority Game}\label{sec:minority_game} 
While simple in its conception, MG has been used in many fields like econophysics~\cite{Challet2008,Bianconi2009,Zapart2009}, multi-agent resource allocation~\cite{Metzler2003,Li2004}, emergence of cooperation~\cite{Dhar2011}, and heterogeneity~\cite{Greenwood2009,Mello2008}. The Minority Game (MG) is intuitively applicable to many domains, such as traffic (the driver that chooses the `less-travelled' road experiences less congestion), packet traffic in networks, ecologies of foraging animals, etc.

\subsection{Description of the Game}\label{subsec:mg_description} 

There is a set of $N = \{1, ..., n\}$ agents (N $=$ odd), each implementing an algorithm $A = \{a_1,...,a_n\}$. At every iteration of the game $G_i$, each agent chooses from a binary set $\{A,B\}$. Thus, at each iteration $i$ of the game ($G$):
\[
G_i (a_j) \rightarrow A\vert B
\]
refers to agent $j$ making a binary choice from $A$ or $B$. If the subset of agents that chose $A$ is smaller than the subset of agents that chose $B$, then the agents that chose $A$ are said to be in the minority ($minG_i$) at iteration $i$. And vice versa. The agents that are in $minG$ for any particular iteration, earn a reward of one unit. The agents that are not in $minG$ receive no reward. If $R_i$ is the reward earned by an agent for a particular iteration, then the total reward earned by an agent after $T$ iterations is given by:
\[
\sum_{i=1}^{T} R_i (a)
\]
Clearly, the allocation of rewards is highest when the size of $minG = N/2$. While the allocation is lowest is when the size of $minG = 1$ (one agent chooses A, and all the rest choose B). 

\subsection{Measuring Algorithm Diversity}
In MG, each agent shares the same functional objective, to continue to be on the side of the minority, and thereby accumulate a reward. The difference in each agent is the algorithm used to compute the best action (A or B). Depending on the type of algorithm used, differences in initialization parameters could lead to different actions chosen (\eg evolutionary or machine learning algorithms). Thus, any algorithm that uses different functional pathways, or is initialized with different parameters, is categorized as a different variety. Given this definition of variety,  the calculation of diversity hinges only on the variety and balance in the population of algorithms. This calculation is most famously done by the \emph{Shannon Index} (eq.~\ref{eq_shannon_index}). The Shannon Index measures the entropy (or uncertainty) in picking an individual of a particular type, from a dataset~\cite{Shannon1948Mathematical}. Thus, for instance, if all the agents in a MAS implement exactly the same algorithm, the entropy (or uncertainty) will be zero. The higher the diversity in the population, the higher is the Shannon Index. In eq.~\ref{eq_shannon_index}, $R$ is the number of types in the population, and $p$ is the proportion of individuals in the population with type $i$.  

\begin{equation}
 H' = -\sum_{i=1}^R p_i \ln p_i
\label{eq_shannon_index}
\end{equation}

The Shannon Index is a measure of both, the \emph{richness of species} (number of different algorithms) and \emph{evenness of species} (abundance of agents implementing an algorithm). We use the Shannon Index to measure how much algorithms differ, in their key parameters. Thus, if a particular initialization parameter (that has an effect on the functional pathway) can take multiple values, the Shannon Index will measure how many such differentiated types exist and how many individuals implement that type, as a proportion of the total population.


\subsection{A Formal Intuition For Distributive Justice}\label{sec:formal_disb_justice}
While \textsl{envy-freeness} is an important concern in many domains, there are some areas, specifically in simultaneous games, where envy-freeness is impossible~\cite{Balkanski2014Simultaneous}. Balkanski~\textit{et al.}~\cite{Balkanski2014Simultaneous} show that for any resource allocation protocol which involves simultaneous choice by more than $3$ agents, it is impossible to construct an envy-free protocol. This impossibility is intuitive in the case of the Minority Game where, by construction, during every round, a majority of the agents are denied a reward and are envious of the minority. \\

In view of the impossibility of an envy-free protocol ever being created, we focus on an alternate form of fairness, \ie \textsl{distributive justice}.\\

\textbf{\textsl{Distributive justice}, in the context of MG, implies the following condition}:
\begin{equation}\label{eqn:distributive-justice} 
\sum_{t=1}^{T} R_t (a_j) \approx \sum_{t=1}^{T} R_t (a_k) \forall (a_k \neq a_j)
\end{equation}

Equation~\ref{eqn:distributive-justice} simply states that, over a period $T$, every agent ($a_j$) in MG must get approximately the same reward ($R(a_j)$), as any other agent ($a_k$). In the ideal case, every agent would get \textbf{exactly} the same reward as every other agent. However, this can be trivially shown to be impossible under the following conditions of agent abilities:

\begin{enumerate}
	\item \emph{Autonomy}: Each agent is autonomous and cannot be coerced by any other agent. The agent's choice is dependent solely on its own algorithm.
	\item \emph{Simultaneity}: Each agent makes its choice in a simultaneous fashion, and no agent has access to any other agent's decision before it makes its own.
\end{enumerate}

%
%It can be trivially shown that in the Minority Game, the minimum number of rounds required to achieve exactly equal rewards for all agents is a constant (\ie $3$). \\


\textbf{Proof:}\\
Assume a set of algorithms $A$ that are implemented in the agents playing MG. If this set $A$ meets two conditions, then MG would be both, efficient and fair:
\begin{enumerate}
	\item The size of the minority is the largest possible, while still being a minority, and \label{condition_1}
	\item The members of the minority group, in any iteration of MG, have never been in a minority before \label{condition_2}
\end{enumerate}
then, after two iterations, there will be only one agent that has never been in a minority before. Therefore, in iteration $3$, the sole agent participates in the minority group, and thus meets the game is both, efficient and fair.

For any mechanism, meeting condition~\ref{condition_1} requires that the size of the  $minG$ be $N/2$ for iteration $1$ \textbf{and} iteration $2$. Meeting condition~\ref{condition_2} requires that the list of agents that have already participated in $minG$ be known at all times, and any agent that has previously been a part of $minG$ be restricted by some mechanism. Meeting  condition~\ref{condition_2} violates the environmental assumption of \textsl{Autonomy}, while meeting condition~\ref{condition_1} violates our environmental assumption of \textsl{Simultaneity}. Thus, there is no way for any mechanism to assure equality of rewards and be efficient, without violating our operating conditions.\\

\textbf{Desirable Properties of a Decentralized Mechanism:}\\
It is evident that equation~\ref{eqn:distributive-justice} would be impossible to achieve with MG, if it were played in a one-shot manner. That is, for a single round of play, regardless of the algorithm chosen, the agents that are in the minority will get a reward, while the agents in the majority will not. This means that equation~\ref{eqn:distributive-justice} will never hold. However, if the same agents play MG in a repeated manner, it is probable that different agents are in a minority in other rounds, such that their rewards accumulated over time would be approximately equal. That is:

\begin{equation}\label{eqn:lim-distributive-justice} 
\lim_{T\to\infty}\sum_{t=1}^{T} R_t (a_j) \approx \sum_{t=1}^{T} R_t (a_k) \forall (a_j \neq a_k)
\end{equation}
Recall, that we are not interested solely in distributive justice, but also that the mechanism should be efficient. In the case of MG, the system would be efficient if allocation of rewards is as high as possible in any given round (see subsection~\ref{subsec:mg_description}). At its most efficient, the game provides the highest amount of reward (when the size of $minG$ is $N/2$):

\begin{equation}\label{eqn:highest-efficiency}
R_{1} = \frac{N}{2}
\end{equation}

This implies that after $T$ rounds of highest possible efficiency, the total rewards in the system must be:

\begin{equation}\label{eqn:total-rewards-highest-efficiency}
\sum_{t=1}^{T} R_t = \frac{N}{2} * T
\end{equation}

From equations~\ref{eqn:highest-efficiency} and \ref{eqn:total-rewards-highest-efficiency}, it is clear that at optimal efficiency, the expected rewards accumulated by any single agent is given by:
\begin{equation}\label{eqn:expected-reward}
\begin{split}
\sum_{t=1}^{T} R_t (a_j) & = \frac{N/2 * T}{N}\\
& = \frac{T}{2}
\end{split}
\end{equation}

Thus, we would like a mechanism that achieves both equation~\ref{eqn:lim-distributive-justice} and \ref{eqn:expected-reward}.

%Given that there is no mechanism of achieving distributive justice in a constant number of rounds, we now look for other mechanisms which do not violate our environmental assumptions and still lead to an even distribution of rewards amongst agents.

%\subsection{Metrics}
%\todo[inline]{Multiple species of algorithms. Calculation of diversity a lot more interesting.}

\section{Experiment}\label{sec:initial_experiment}
The results presented in this section were first shown in \cite{Nallur2016Algorithm}, and hence this section presents only a summary. Diversification of algorithms was performed on two levels: \emph{(a)} Parameter Diversification, and \emph{(b)} Strategy Diversification\footnote{All code and data for this experiment is available at\\ \url{https://bitbucket.org/viveknallur/aamas2016.git}}. Three algorithms were chosen as the candidate algorithms:
\begin{enumerate}
	\item \textbf{BestPlay}: Introduced by the creators of the MinorityGame, this is a simple strategy that nevertheless exhibits interesting dynamics at the aggregate level~\cite{Challet1997}. In this strategy, all players had access to a bounded collective memory ($m$) of the game, along with a pool of strategies($s$). Collective memory is implemented in the form of a history of which action (+1 or -1) was in the minority, at each timestep. Each player drew $s$ strategies randomly from a strategy space, that indicated which action to play next, based on the history of the game. Each player allocated virtual points amongst all its strategies, based on the continually changing history of the game. Strategies that correctly predicted the correct output, regardless of whether they were used or not, received virtual points. Each player, for the next round, used the top-ranking strategy amongst his pool of strategies. Table~\ref{mg_sample_strategies} shows a sample strategy pool used by a player. The size of each strategy depends on $m$, the memory of the game, while the total number of strategies in the pool depends on the computational power the agent possesses. Table~\ref{mg_sample_strategies} is therefore taken from a game with a memory of size 3, while the agent has a pool size ($k$) of 4. Typically, each agent in the entire game has the same pool size ($k$). While it is intuitively obvious that the greater the pool size, the greater the probability of an agent drawing good strategies, it is impossible for an agent to have a pool size that exhaustively covers every possible strategy, since the strategy space grows hyper-exponentially($2^{2^{m}}$) with increase in $m$.

\begin{table}[ht]
\centering
\def\arraystretch{1.5}
\begin{tabular}{|l|r|}
\hline
\textbf{Strategy} & \textbf{Output} \\ \hline
{[}+1 -1 +1{]}    & -1              \\ \hline
{[}-1 -1 -1{]}    & -1              \\ \hline
{[}-1 -1 +1{]}    & +1              \\ \hline
{[}+1 +1 +1{]}    & -1                \\ \hline
\end{tabular}
\caption{Sample Strategy Pool for a Player}
\label{mg_sample_strategies}
\end{table}

	\item \textbf{Evolutionary}: Evolutionary algorithms have been used quite successfully in multiple domains~\cite{Ashlock2006, DeJong2006}. As pointed out in~\cite{Eiben2015}, evolutionary algorithms are now being compared to human beings, in their ability to solve hard problems. We have implemented an evolutionary algorithm that uses \emph{BestPlay}'s strategy representation as a genome and performs crossover and mutation on it. The strategy is as follows: At every reproduction cycle, the ten most poorly performing agents get rid of their strategies and adopt a combination of strategies from two parents randomly selected from the ten best performing agents. Effectively, two of the ten best performing agents have reproduced to create an offspring that replaces one of the ten worst agents. The offspring mutates the strategies obtained from its parents, by some small mutation probability. This allows the offspring to get better, as well as explore more of the strategy space. The reproduction cycle refers to the rounds after which evolution occurs. A frequency of $20$ cycles means that after every 20 rounds of the game, the worst 10 agents discard their strategies and inherit good strategies from $2$ of the top 10 agents. These are then mutated according to the agents' own mutation probability.
	
	\item \textbf{Roth-Erev}: We implement a Reinforcement-Learning algorithm, called Roth-Erev~\cite{Roth1995}. Reinforcement-Learning(RL) is a class of machine-learning algorithms where an agent tries to learn the optimal action to take, in a certain environment, based on a succession of \textit{action-reward} pairs. 
	Given a set of actions (a) that an agent can take, the Roth-Erev learning algorithm performs the following steps:
	\begin{enumerate}
		\item Initialize initial propensities~($q$) amongst all actions~($N$)
		\item Initialize strength ($s$) of initial propensities
		\item Initialize recency ($\varphi$) as the `forgetting' parameter
		\item Initialize epsilon ($\varepsilon$) as the exploration parameter
		\item Choose action ($a_{k}$) at time ($t$), based on $q$ \label{rl_action}
		\item Based on reward($r_{k}$) for $a_{k}$, update $q$ using the following formula:
		\[
			q_{j}(t + 1) = [1 - \varphi]q_{j}(t) + E_{j}(\varepsilon,N, k, t)
		\]
		\begin{align*}
		E_{j}(\varepsilon,N, k, t) &=
		\begin{cases}
			r_{k}(t)[1 - \varepsilon]          & \text{if } j = k \\
			r_{k}(t)\frac{\varepsilon}{N - 1}  & \text{otherwise}
		\end{cases} 
		\end{align*}
	
		\item Probability of choosing action $j$ at time ($t$) is given by:
	\[
		P_{j}(t) = \frac{q_{j}(t)}{\sum_{n=1}^{N}[q_{n}(t)]}
	\]
		\item Repeat from step~\ref{rl_action}
	\end{enumerate}

\end{enumerate}

\subsection{Parameter Diversity}
Parameter diversity refers to diversity in the initialization parameters of the algorithm being implemented by the agents. That is, for any game, all agents still implement the same code, but some parameter in the algorithm is diversified. For the Evolutionary and Roth-Erev algorithms, even minute differences within the initialization parameters can cause aggregate average payoff to vary. For each of the three families, the following parameters were diversified within the algorithm:
	\begin{enumerate}
		\item Best Play algorithm: $k$ -- which is the pool of strategies, a player has(strategies-per-agent). 
		\item Evolutionary algorithm: The mutation probability that allows an agent to explore the strategy space, by mutating known good strategies. 
		\item Roth-Erev algorithm: The parameters of \emph{recency} ($\varphi$), and \emph{exploration} ($\varepsilon$)
	\end{enumerate}


\subsection{Strategy Diversity}
We mix agents implementing different algorithms (strategies) into one population. The diversification here takes place on two levels, parameter-diversification (as before), as well as proportion of population implementing a particular strategy. The total population of agents in all scenarios is kept constant, but the proportion of agents playing a particular strategy is varied. This leads to different levels of diversification, for each combination of strategies. The parameters that were varied were as follows:
\begin{enumerate}
	\item \textbf{Number of Algorithms}: This reflects the effective number of families (or \textit{genera}) of species that are present in the population.
	\item \textbf{Proportion of population implementing an algorithm}: Depending on the relative number of each specie, the aggregate reward in the game changes.
	\item \textbf{Parameters within algorithms that were diversified}: Variation in critical parameters for an algorithm results in effectively creating a new specie, since the functional pathway (and resultant output) begins to change depending on the environment. 
\end{enumerate}

Table~\ref{experimental_constants} shows the constant factors in each experiment. Each minority game was played with a population size of $501$ ($N$) agents, through a simulation time period of $2000$ steps ($T$). For each variation in the experimental setup, the data is reported as an average of 100 simulations.

\begin{table}[ht!]
\centering
%\large
\def\arraystretch{1.5}
\begin{tabular}{|l|c|}
%\toprule
\hline
{\bf Variable}       & {\bf Value} \\ \hline
Population Size      & 501         \\ \hline
Simulation Period (rounds)    & 2000        \\ \hline
Repetition of Variation		   & 100  \\ \hline
\end{tabular}
\caption{Experimental constants for Minority Game}
\label{experimental_constants}
\end{table}

\subsection{Results}
 
We combine the results from all of the diversification to illustrate the comparative rewards achieved at different levels of diversity. The graphs show the distribution of the population achieving a particular level of average reward. The `fatter' the graph, the more evenly rewards are distributed, and vice-versa. Figure~\ref{all_together} clearly shows that the higher the diversity level, the fairer the average reward. Note that the highest level ($5.13$) was \textbf{not} a consequence of mixing all three algorithms, but rather by mixing Evolutionary and Roth-Erev algorithm. This confirms our intuition that merely mixing more algorithms does not lead to greater diversity.

\begin{figure}
\centering
\includegraphics[scale=0.56,clip,trim={0cm 0cm 3cm 0cm}]{figures/mas/strategy-diversity/allTogether.pdf}
\caption{Payoff across all diversification}
\label{all_together}
\end{figure}

%\subsubsection{Normal Play --- Baseline}
%In Figure~\ref{simple_play_best_play}, we see the average payoff (y-axis) from the Best-Play strategy, with different levels of memory(x-axis). The average payoff is the average number of times, an agent is able to make the correct choice and be in the minority. The graph shows the distribution of population that was able to secure rewards, during the entire game. The \emph{fatter} the distribution, the fairer the result, since it implies that, on average, most players secured the same amount of payoff. In Figure~\ref{simple_play_best_play}, we see that with a memory of $8$, the distribution of payoff is more equitable, since most of the population is concentrated in one area.
%\begin{figure}
%\centering
%\includegraphics[scale=0.45]{figures/mas/simple-play/bestPlaySimple.pdf}
%\caption{Best-Play strategy with different values of memory}
%\label{simple_play_best_play}
%\end{figure}
%
%\begin{figure}
%\centering
%\includegraphics[scale=0.5]{figures/mas/simple-play/evoSimple.pdf}
%\caption{Evolutionary strategy}
%\label{simple_play_evo}
%\end{figure}
%
%\begin{figure}
%\centering
%\includegraphics[scale=0.5]{figures/mas/simple-play/rotherSimple.pdf}
%\caption{Roth-Erev strategy}
%\label{simple_play_rother}
%\end{figure}
%
%In Figure~\ref{simple_play_evo}, we see the average payoff from the Evolutionary strategy. The x-axis shows the frequency of the reproduction cycle, \textit{i.e.}, reproduction and mutation takes place every $20, 40, 60, 80$ and $100$ rounds. Figure~\ref{simple_play_evo} clearly shows that when reproduction and mutation is frequent (every $20$ rounds), more of the population earns a higher payoff. That is, most of the population earns a payoff between $700$ and $1000$, whereas with reproduction cycles of $60$ and greater, most of the population earns a lower reward, with a few individuals earning higher payoff.
%
%Figure~\ref{simple_play_rother} shows the average payoff when the Roth-Erev strategy is used. Again, the fatter the distribution, the fairer the outcome. Hence, we see that low values of \emph{Recency} ($0.1, 0.2$) contribute to a fairer distribution of payoff. 
%
%In all of these, the diversity of the population is \emph{zero} ($0$), since there is only one algorithm, and all agents implement exactly the same code.

%The specific parameters being diversified are given in section~\ref{sec:initial_experiment}. 
%\begin{figure}
%\centering
%\includegraphics[scale=0.4, clip, trim={0 0 3cm 0}]{figures/mas/param-diversity/bestPlay.pdf}
%\caption{Parameter Diversification of Best Play}
%\label{param_diverse_best_play}
%\end{figure}
%
%\begin{figure}
%\centering
%\includegraphics[scale=0.4, clip, trim={0 0 3.5cm 0}]{figures/mas/param-diversity/evo.pdf}
%\caption{Parameter Diversification of Evolutionary strategy}
%\label{param_diverse_evo}
%\end{figure}
%
%\begin{figure}[ht]
%\centering
%\includegraphics[scale=0.4, clip, trim={0 0 3cm 0}]{figures/mas/param-diversity/rother.pdf}
%\caption{Parameter Diversification of Roth-Erev strategy}
%\label{param_diverse_rother}
%\end{figure}

%\paragraph{Parameter Diversity with Best Play}
%In general, there is less parameter diversity with the Best Play strategy, since the parameter being diversified is the number of strategies that player holds. 

%These are drawn from a gaussian distribution with the mean being the size of the history available to agents, and a standard deviation of $1$. This leads to there being a low diversity, since many agents end up with the same amount of strategies-per-agent. Figure~\ref{param_diverse_best_play} therefore shows very little difference in distribution of payoffs obtained, as compared to Figure~\ref{simple_play_best_play}. 
%

%\paragraph{Parameter Diversity with Evolutionary Strategy}
%The evolutionary strategy changes the performance of the agents, depending on the strategies that the `evolved' child agents get. The evolutionary process allows poorly performing agents to quickly change their strategies to match the best agents. However, this also leads to a homogeneity in the strategies being used by the agents. The parameter that diversifies each agent is the mutation probability that it possesses. This causes agents to mutate their inherited strategies. Figure~\ref{param_diverse_evo} shows the difference in average payoff, when the reproduction cycle is varied. 

%\paragraph{Parameter Diversity with Roth-Erev}
%Roth-Erev allows for the highest amount of diversification, since there are two parameters (recency and epsilon) that can be changed for each agent. Also, the algorithm is extremely sensitive to the values of these parameters, and each agent's decision-making is affected significantly. Hence, the population as a whole becomes extremely diverse. Figure~\ref{param_diverse_rother} shows the effect of diversification for mean values of recency and epsilon.

%Table~\ref{tbl_diversity_due_to_param} shows the relative diversity in population generated due to parameter diversification. 
%Both Roth-Erev and the Evolutionary strategy exhibit higher levels of diversity, due to the probabilistic nature of the variable being diversified. In the Evolutionary strategy, the mutation-probability is different for every agent, thereby making each agent into its own separate type. In Roth-Erev, \emph{both} recency and epsilon are diversified, which results in a higher level of diversity. This increase in diversity is reflected in both, the fairness of the average payoff, and the levels of average payoff. That is, for Roth-Erev populations, most of the payoff is at the $800$ level, while for the Evolutionary strategy, most of the payoff is around $500$.
%\begin{table}
%\centering
%\begin{tabular}{|l|l|l|}
%\hline
%\textbf{Strategy} & \textbf{Min Diversity} & \textbf{Max Diversity} \\ \hline
%Best-Play         & 0.60               & 0.64               \\ \hline
%Evolutionary      & 2.69               & 2.69               \\ \hline
%Roth-Erev         & 5.39               & 5.39               \\ \hline
%\end{tabular}
%\caption{Diversity Generated Due to Parameter Diversification}
%\label{tbl_diversity_due_to_param}
%\end{table}

%\begin{figure}
%\centering
%\includegraphics[scale=0.4,clip=yes, trim=5cm 0 5cm 0cm]{figures/mas/strategy-diversity/evoBest.pdf}
%\caption{Mix of Best-Play and Evolutionary Strategies}
%\label{bp_plus_evo}
%\end{figure}

% We show only the population mixes with the \emph{lowest} and \emph{highest} amount of diversity, for each combination of strategies. The x-axis shows the diversity levels (as measured by the Shannon Index) and the y-axis, the distribution of average payoff.

%\begin{figure}
%\centering
%\includegraphics[scale=0.34,clip=yes, trim=1cm 0 0 2cm]{figures/mas/strategy-diversity/rl_Best_minmax.pdf}
%\caption{Mix of Best-Play and Roth-Erev Strategies}
%\label{bp_plus_rother}
%\end{figure}

%\paragraph{Best Play and Evolutionary}
%Figure~\ref{bp_plus_evo} shows the distribution of payoffs when a set of agents playing Best Play, is mixed with a set of agents playing the Evolutionary strategy. Figure~\ref{bp_plus_rother} shows the distribution of payoffs when a set of agents playing Best Play, is mixed with a set of agents playing the Roth-Erev strategy. Figure~\ref{evo_plus_rother} shows the distribution of payoffs when a set of agents playing Evolutionary strategy, is mixed with a set of agents playing the Roth-Erev strategy. Figure~\ref{bp_plus_evo_plus_rother} shows the distribution of payoffs, when all three strategies are combined into one population. 
%
%\begin{figure}
%\centering
%\includegraphics[scale=0.4,clip=yes, trim=1cm 0 5cm 0cm]{figures/mas/strategy-diversity/evoRother.pdf}
%\caption{Mix of Evolutionary and Roth-Erev Strategies}
%\label{evo_plus_rother}
%\end{figure}
%
%\begin{figure}
%\centering
%\includegraphics[scale=0.34,clip=yes, trim=1cm 0 0 1.2cm]{figures/mas/strategy-diversity/evoRlBestPlay.pdf}
%\caption{Mix of three strategies}
%\label{bp_plus_evo_plus_rother}
%\end{figure}




\subsection{Effects of Algorithmic Diversity}
As was pointed out earlier in this section, MG represents a class of games where envy-freeness is impossible to achieve. After any given round, a majority of the agents are envious of the allocations secured by the minority. Algorithmic Diversification offers an alternate resolution, in such games, where multiple iterations of the game result in approximate envy-freeness. That is, on multiple iterations of an envious game, the introduction of algorithmic diversity, moves all the agents towards receiving approximately the same amount of reward As seen in Figure~\ref{all_together}, the increase in diversity results in increase in fairness, which can be seen as a proxy for envy-freeness. This occurs due to a 'rotation' of agents that receive a reward, through the multiple iterations. The faster this rotation, in allowing agents that have previously not been rewarded to receive rewards, the more the likelihood of envy-freeness. \\


\ifdefined\build\else\input{tail}\fi