\begin{appendices}

\section{}\label{sec:appendix-cp-for-evo}
The clonal plasticity process step-by-step for \textit{Evo} is as follows:
\begin{enumerate}
	\item \textbf{Identify Plasticity Points}: The Evo algorithm diversifies itself based on the mutation probability of an individual. Two agents that diversify into different directions (one increasing the mutation probability, and the other decreasing) could end up with drastically different strategies after their evolutionary reproduction. Here, $ownMutationRate$ is identified as a plasticity point of the algorithm.  
	
	\item \textbf{Evaluate Environmental Input}: For any given time instant, the algorithm has a $50$\% chance of picking the right group (\textit{i.e.}, being in the minority). Hence, for any given period of cloning frequency, if the agent has accumulated (\texttt{wins-this-cycle}) more than $50$\% of the rewards during the period, the feedback is said to be positive. If the agent accumulates less than $50$\% of the rewards, then the feedback is negative. 
	
	\item \textbf{Plasticity Memory}: The agent keeps track of the rewards that it had accumulated during the previous adaptation (\texttt{wins-last-cycle}). Along with the feedback, this helps it decide what plasticity range to choose next (see Table~\ref{tbl:plasticity-range-criteria-evo}). 
	
	\item \textbf{Clone and Modify Plastic Points}: In the event of \emph{Exact Clone}, the agent does not change its plasticity point. In the event of \emph{Low Plasticity}, the agent modifies its $ownMutationRate$ in the same direction, as the plasticity memory from the previous generation. In the event of \emph{High Plasticity}, the agent modifies its $ownMutationRate$ to return to the value that it had from the previous generation.
	
\end{enumerate}

\begin{table}[]
\def\arraystretch{1.5}
\begin{tabular}{|l|l|l|}
\hline
\textbf{Feedback} & \textbf{Plasticity Memory}                   & \textbf{\begin{tabular}[c]{@{}l@{}}Plasticity Range\\ Chosen\end{tabular}} \\ \hline
Negative          & (Disregarded)                                & High Plasticity                                                             \\ \hline
Positive          & wins-this-cycle \textless= wins-last-cycle   & Low Plasticity                                                                \\ \hline
Positive          & wins-this-cycle \textgreater ~ wins-last-cycle & Exact Clone                                                                \\ \hline

	\end{tabular}
	\caption{Criteria for choosing plasticity range}
	\label{tbl:plasticity-range-criteria-evo}
	\end{table}

\section{}\label{sec:appendix-cp-for-re}
The clonal plasticity process step-by-step for \textit{RothErev} is as follows:
\begin{enumerate}
	\item \textbf{Identify Plasticity Points}: The RothErev algorithm  depends on two variables, $Recency$ and $Epsilon$. These affect how much weight the algorithm gives to the recent past, and how much exploration it does, respectively. Any difference in the values of these variables can cause agents that start out the same, to diverge in their strategies.
		
	\item \textbf{Evaluate Environmental Input}: For any given time instant, the algorithm has a $50$\% chance of picking the right group (\textit{i.e.}, being in the minority). Hence, for any given period of cloning frequency, if the agent has accumulated (\texttt{wins-this-cycle}) more than $50$\% of the rewards during the period, the feedback is said to be positive. If the agent accumulates less than $50$\% of the rewards, then the feedback is negative. 
	
	\item \textbf{Plasticity Memory}: The agent keeps track of the rewards that it had accumulated during the previous adaptation (\texttt{wins-last-cycle}). Along with the feedback, this helps it decide what plasticity range to choose next (see Table~\ref{tbl:plasticity-range-criteria-evo}). 
	
	\item \textbf{Clone and Modify Plastic Points}: In the event of \emph{Exact Clone}, the agent does not change any of its parameters. In the event of \emph{Low Plasticity}, the agent picks one of its plastic points and modifies it in the same direction, as the plasticity memory from the previous generation. In the event of \emph{High Plasticity}, the agent modifies \textit{all} its plasticity points to return to the values that it had from the previous generation.
	
\end{enumerate}

%\paragraph{}
Note that in this paper, we have differentiated the algorithms based only on their plasticity points. It is also possible to modify the rules by which the environmental input is evaluated and how the plasticity range is chosen, for each individual algorithm. 
%\begin{table}[]
%\def\arraystretch{1.5}
%\begin{tabular}{|l|l|l|}
%\hline
%\textbf{Feedback} & \textbf{Plasticity Memory}                   & \textbf{\begin{tabular}[c]{@{}l@{}}Plasticity Range\\ Chosen\end{tabular}} \\ \hline
%Negative          & (Disregarded)                                & High Plasticity                                                             \\ \hline
%Positive          & wins-this-cycle \textless= wins-last-cycle   & Low Plasticity                                                                \\ \hline
%Positive          & wins-this-cycle \textgreater ~ wins-last-cycle & Exact Clone                                                                \\ \hline
%
%	\end{tabular}
%	\caption{Criteria for choosing plasticity range for \textit{RothErev}}
%	\label{tbl:plasticity-range-criteria-re}
%	\end{table}

%\section{}%{Appendix A}
%We show an algorithm-wise breakup of diversity achieved, as well as the equity achieved. Figures~\ref{fig:algo-wise-reward} and \ref{fig:algo-wise-gini} show the differences in algorithm mixes, that resulted in differences in reward achieved as well as the Gini index. Read in conjunction with Figure~\ref{fig:algo-wise-num-species}, which shows that three algorithms (\texttt{Evo, RothErev} and \texttt{RothErev-Evo}) achieved the highest diversity, it shows that high levels of diversity are linked to high levels of reward. Table~\ref{tbl:full_table}, from Appendix~\ref{sec:appendix-b} shows how this can be further broken down based on how rapidly the cloning process operates.
%
%\begin{figure}
%\centering
%\includegraphics[scale=0.45,clip,trim={0 0.76cm 0 0.76cm}]{figures/clonal-plasticity/reward-vs-algorithm.png}
%\caption{Algorithm-wise breakup of Reward achieved}
%\label{fig:algo-wise-reward}
%\end{figure}
%
%\begin{figure}
%\centering
%\includegraphics[scale=0.45,clip,trim={0 0.76cm 0 0.76cm}]{figures/clonal-plasticity/gini-vs-algorithm.png}
%\caption{Algorithm-wise breakup of Gini Index}
%\label{fig:algo-wise-gini}
%\end{figure}
%
%\begin{figure}
%\centering
%\includegraphics[scale=0.45,clip,trim={0 0.76cm 0 0.76cm}]{figures/clonal-plasticity/numSpecies-vs-algorithm.png}
%\caption{Algorithm-wise breakup of number of species at the end of cloning}
%\label{fig:algo-wise-num-species}
%\end{figure}




\section{}\label{sec:appendix-b}

In this section, we show detailed information about how cloning frequency affects both, the diversity in the population as well as the \textit{Gini} index. Recall that the Gini index measures the equity of reward in a society. The closer the absolute value of the index is to zero, the more equitable a society is. Conversely, the higher the Gini index, the less equitable distribution of rewards in that society. Table~\ref{tbl:full_table} shows the mean diversity, rewards and gini index for each cloning frequency, for each mix of algorithm. The final column (\texttt{std dev}) records the standard deviation for the rewards achieved. Note that each population with a single algorithm (\texttt{BestPlay, RothErev and Evo}) has a different cloning frequency, while each population with a mix of algorithms has a common frequency ($60,120,180,240,300$). This is due to the fact that \texttt{Evo} requires at least $20$ rounds for speciation to show a difference in behaviour.

\begin{table*}
\centering
\def\arraystretch{1.5}
\input{algo-clonecycle-div-reward-gini-std}

\caption{All mixes of algorithms and cloning frequencies and their associated diversity, gini, reward values}

\label{tbl:full_table}

\end{table*}




\end{appendices}