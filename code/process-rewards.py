import contextlib
import collections
import errno
import matplotlib.pyplot as plt
import numpy as np
import optparse
import os
import pandas as pd
import seaborn as sns


def calc_gini(rewards):
    """
    calculate the gini coefficient of rewards obtained by a population. Gini
    coefficient measures the 'inequality' of incomes in a population.
    Based on the Gini formula given at:
        https://en.wikipedia.org/wiki/Gini_coefficient
    """
    # requires all values in rewards to be zero or positive numbers,
    # otherwise results are undefined
    n = len(rewards)
    summation_of_rewards = rewards.sum()
    rank = np.argsort(np.argsort(rewards)) # calculates zero-based ranks
    numerator = 2.0 * (rank * summation_of_rewards).sum()
    denominator = n * summation_of_rewards
    #G = (numerator / denominator) - ((n + 1) / n)
    #G = 1 + ( 1 / n ) - ( numerator / denominator )
    G = 1 - (numerator + summation_of_rewards)/(denominator)
    return G


def process_line(line, combination):
    """
    This function processes the actual data line. It performs the following
    functions:
        a) collects the value of combination fields into a tuple
        b) collects the last field which is data, strips the square brackets
        surrounding it, and makes a list of ints out of the data
        c) returns tuple, data
    """
    translator = str.maketrans({'"':None})  ##  Makes use of the fact that translate
    # is much faster than any other string sub method

    line_data = [i.translate(translator) for i in line.split(",")]
    combo = []
    for field in combination:
        combo.append(line_data[field])
    data = str(line_data[-1])
    translator = str.maketrans({'[':None, ']':None})
    data = data.translate(translator)
    data = [int(d) for d in data.split()] # split on space and convert to ints
    data = np.array(data) # convert to numpy array for statistical manipulation
    return tuple(combo), data


def process_file(filename, combination):
    """
    This function actually starts processing a specific data file. It assumes
    that the filename passed in, consists of an absolute path, since all
    statistical graphs and files will be generated in the current working directory

    """
    print("Checking that my working directory is: %s" % (os.getcwd()))
    print("Checking that file that I'm reading is: %s" % (filename))
    all_data = collections.defaultdict(list)
    header_lines = 6
    for _ in range(header_lines):
        next(filename) # skip header_lines
    field_line = next(filename) # contains the field names
    field_line = field_line.split(",")
    field_names = []
    for item in combination:
        field_names.append(field_line[item])
    print(repr(field_names))
    # slurp the data in the file and add it to all_data
    for line in filename:
        combo, combo_data = process_line(line, combination)
        all_data[combo].append(combo_data)

    # write out processed information into a new file
    with open ('experiment_data.csv', 'w') as cf:
        # write the header line
        header_line = ','.join(map(str, field_names))
        header_line = ','.join([header_line,"median_reward,mean,variance,gini"])
        header_line = ''.join([header_line, "\n"]) # No trailing comma before newline
        cf.write(header_line)
        for combo, data in all_data.items():
            for observation in data:
                cf.write(','.join(map(str, combo)))
                cf.write(",%d,%f,%f,%f\n" % (np.median(observation), np.mean(observation), np.var(observation), calc_gini(observation)))



def process_files(filename, combination, xaxis, output_file):
    combination = [int(i) for i in combination.split(",")]
    created_dir = create_dir(filename)
    with open(filename, 'r') as file:
        with working_directory(created_dir) as wd:
            print("Changed working directory to: %s" % (os.getcwd()))
            process_file(file, combination)
            sns.set_style("darkgrid")
            experiment_data = pd.read_csv("experiment_data.csv")
            experiment_data.sort_values(xaxis, inplace=True)
            ax = sns.violinplot(x=xaxis, y="median_reward", data=experiment_data)
            plt.savefig(output_file)


@contextlib.contextmanager
def working_directory(path):
    """
    changes working directory temporarily and switches back upon completion
    """
    prev_path = os.getcwd()
    os.chdir(path)
    try:
        yield
    finally:
        os.chdir(prev_path)


def create_dir(filename):
    dirname_to_create = os.path.basename(os.path.abspath(filename))
    dirname_to_create = dirname_to_create.split('.', 1)[0]
    dirname_to_create = ''.join([dirname_to_create, "-graphs"])
    path_to_create = os.path.dirname(os.path.abspath(filename))
    dirname_to_create = ''.join([path_to_create, os.sep, dirname_to_create])
    print("dirname to be created: %s" % (dirname_to_create))
    try:
        os.makedirs(dirname_to_create)
    except OSError as exception:
        if exception.errno != errno.EEXIST:
            raise
    finally:
        return dirname_to_create


if __name__ == '__main__':
    """
    First we parse the arguments, which consist of:
    a) the datafile (must be a ".csv" file)
    b) the fields in the datafile that form a combination
    """
    parser = optparse.OptionParser()
    parser.add_option("-f","--file", dest="filename", help="file that is to be processed", action="store", type="string")
    parser.add_option("-c","--combo", dest="combination", help="combination of fields to be processed. E.g. -- \"1\" or \"1,2,3\"", action="store", type="string")
    parser.add_option("-x","--xaxis", dest="xaxis", help="value that will be on the x-axis for graphing", action="store", type="string")
    parser.add_option("-o","--output", dest="output_file", help="file that is to be written to", action="store", type="string")
    (options, args) = parser.parse_args()
    process_files(options.filename, options.combination, options.xaxis,options.output_file)
