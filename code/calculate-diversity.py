import collections
import contextlib
import itertools
import math
import optparse
import os
import pandas as pd


def calc_shannon(pop):
    TOTAL_POPULATION = 501
    H = 0
    #print(repr(pop))
    specie_proportion = collections.Counter(pop)
    num_species = len(specie_proportion)
    for k,v in specie_proportion.items():
        #print("v = %d, total = %d"%(v, TOTAL_POPULATION))
        prop = v/TOTAL_POPULATION
        #print("Prop = %f"%(prop))
        H = H + (prop * math.log10(prop))
    H = -H
    print("Shannon is: %f"%(H))
    #print("Number of species: %d"%(num_species))
    #indiv_per_specie = TOTAL_POPULATION / num_species
    #print("Number of individuals per specie: %d"%(indiv_per_specie))
    return (H,num_species)


def process_specie_line(specie_lines):
    """
    * check if this is a start or an end
    * for each line, remove the start and ending '[]'
    * combine all lines such that a space separates each individual
    * send the combined line to calc_shannon to get H-index
    * create dict with field_names as key and H-index as value
    """
    translator = str.maketrans({'[': None, ']':None, '\n':None})  # makes use of the fact
    position = specie_lines[0].translate(translator) # that translate works faster than lstrip
    #print("position is: %s" % (position))
    key = specie_lines[1].translate(translator)
    print("key is: %s" % (key))
    #print ("specie is: %s" % (specie_lines[1]))
    population = []
    for idx in range(2,len(specie_lines)):
         population.append(specie_lines[idx].translate(translator).split(" "))
    population = list(itertools.chain.from_iterable(population)) # combine all lines into one
    population = filter(None, population)  # Remove empty strings
    (h_index,num_species) = calc_shannon(population)
    return position,{key:(h_index, num_species)}


def process_files(filename, num_specie_lines,field_names):
    created_dir = find_dir(filename)
    num_specie_lines = int(num_specie_lines)
    diversity_start_info = []
    diversity_end_info = []
    combined_diversity_info = collections.defaultdict(list)
    with open(filename, 'r') as speciefile:
        with working_directory(created_dir) as wd:
            print("Changed working directory to: %s" % (os.getcwd()))
            with open ('diversity_data.csv.new', 'w') as datafile:
                header_line = ','.join(map(str,field_names))
                header_line = ','.join([header_line,'starting_diversity','num_starting_species', 'ending_diversity','num_ending_species'])
                datafile.write(header_line)
                datafile.write("\n")
                while True:
                    try:
                        specie_lines = []
                        for lineno in range(num_specie_lines):
                            specie_lines.append(next(speciefile))
                        position, diversity_val = process_specie_line(specie_lines)
                        #print("Received position: %s" % (repr(position)))
                        if position == 'start':
                            diversity_start_info.append(diversity_val)
                            #print ("Adding for start position: %s" %(position))
                        else:
                            diversity_end_info.append(diversity_val)
                            #print ("Adding for end position: %s" %(repr(position)))
                    except StopIteration:
                        break
                for diversity_tuple in zip(diversity_start_info, diversity_end_info):
                    #print("diversity_tuple: %s"%(repr(diversity_tuple)))
                    combination = list(diversity_tuple[0]) # we know there will be only one key
                    #print("combination: %s"%(repr(combination)))
                    dataline = combination[0] # so extract keys as list and get the first element
                    for indiv_dict in diversity_tuple:
                        for key,value in indiv_dict.items():
                            div_and_species = ','.join([str(i) for i in value])
                            dataline = ','.join([dataline, div_and_species])
                    dataline = ''.join([dataline, '\n'])
                    datafile.write(dataline)




@contextlib.contextmanager
def working_directory(path):
    """
    changes working directory temporarily and switches back upon completion
    """
    prev_path = os.getcwd()
    os.chdir(path)
    try:
        yield
    finally:
        os.chdir(prev_path)


def find_dir(filename):
    created_dirname = os.path.basename(os.path.abspath(filename))
    created_dirname = created_dirname.split('.', 1)[0]
    created_dirname = ''.join([created_dirname, "-graphs"])
    path_to_create = os.path.dirname(os.path.abspath(filename))
    created_dirname = ''.join([path_to_create, os.sep, created_dirname])
    print("already created directory: %s" % (created_dirname))
    try:
        if os.path.isdir(created_dirname):
            return created_dirname
        else:
            raise "Directory not found!"
    except:
        print ("This should never print")


if __name__ == '__main__':
    """
    First we parse the arguments, which consist of:
    a) the datafile (must be a ".csv" file. If not given, default is 'experiment_data.csv')
    b) the fields in the datafile that form a combination
    c) one of the fields out of the combinations above, that can be used as the
    x-axis of the violin plot generated. The y-axis is always 'median_reward'
    d) the name of the output file generated. The file is usually generated
    in pdf format
    """
    parser = optparse.OptionParser()
    parser.add_option("-f","--file", dest="filename", help="file that is to be processed", action="store", type="string")
    parser.add_option("-c","--count", dest="count", help="number of physical lines that make up a specie count", action="store", type="string")
    parser.add_option("-n","--names", dest="field_names", help="field names present in the species file", action="store", type="string")
    (options, args) = parser.parse_args()
    num_specie_lines = int(options.count)
    field_names = options.field_names.split(",")
    process_files(options.filename, num_specie_lines, field_names)
