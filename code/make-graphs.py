import contextlib
import matplotlib.pyplot as plt
import optparse
import os
import pandas as pd
import seaborn as sns


def process_files(filename):
    created_dir = find_dir(filename)
    calculated_columns = ['median_reward', 'mean', 'variance', 'gini']
    with open(filename, 'r') as file:
        with working_directory(created_dir) as wd:
            print("Changed working directory to: %s" % (os.getcwd()))
            sns.set_style("darkgrid")
            sns.set_context("paper")
            experiment_data = pd.read_csv("experiment_data.csv")
            column_list = experiment_data.columns.values.tolist()
            for column in column_list:
                if column not in calculated_columns:
                    output_file = ''.join([column, ".pdf"])
                    experiment_data.sort_values(column, inplace=True)
                    ax = sns.violinplot(x=column, y="median_reward", data=experiment_data, cut=0,scale='area', inner='box', palette='Set1')
                    plt.savefig(output_file)
                else:
                    print ("%s is a calculated column, so won't be graphed" %(column))

def clone_vs_diversity(filename, column1,column2):
    created_dir = find_dir(filename)
    with open(filename, 'r') as file:
        with working_directory(created_dir) as wd:
            print("Changed working directory to: %s" % (os.getcwd()))
            sns.set_style("darkgrid")
            sns.set_context("paper")
            diversity_data = pd.read_csv("diversity_data.csv")
            #output_file = ''.join([column, ".pdf"])
            output_file = 'BP-Evo-AchievedDiversity.pdf'
            fig, ax = plt.subplots()
            ax = sns.pointplot(x="ending_diversity", y=column1, data=diversity_data, ci=95, capsize=0.2, ax=ax)
            ax.set(xlabel='Diversity Achieved',ylabel='Evo Proportion')#, ylim=(2.65, 2.8))
            ax2 = ax.twinx()
            ax = sn.pointplot(x="ending_diversity", y=column2, data=diversity_data, ci=95, ax=ax2)
            ax.set(xlabel='Diversity Achieved', ylabel='Cloning Frequency')
            plt.savefig(output_file)


def strategy_vs_gini(filename, column):
    created_dir = find_dir(filename)
    with open(filename, 'r') as file:
        with working_directory(created_dir) as wd:
            print("Changed working directory to: %s" % (os.getcwd()))
            sns.set_style("darkgrid")
            sns.set_context("paper")
            experiment_data = pd.read_csv("experiment_data.csv")
            output_file = ''.join([column, ".pdf"])
            ax = sns.pointplot(x=column, y="gini", data=experiment_data, ci=95, capsize=0.2)
            ax.set(xlabel=str.capitalize(column), ylabel='Gini Index')#, ylim=(2.65, 2.8))
            plt.savefig(output_file)



@contextlib.contextmanager
def working_directory(path):
    """
    changes working directory temporarily and switches back upon completion
    """
    prev_path = os.getcwd()
    os.chdir(path)
    try:
        yield
    finally:
        os.chdir(prev_path)


def find_dir(filename):
    created_dirname = os.path.basename(os.path.abspath(filename))
    created_dirname = created_dirname.split('.', 1)[0]
    created_dirname = ''.join([created_dirname, "-graphs"])
    path_to_create = os.path.dirname(os.path.abspath(filename))
    created_dirname = ''.join([path_to_create, os.sep, created_dirname])
    print("already created directory: %s" % (created_dirname))
    try:
        if os.path.isdir(created_dirname):
            return created_dirname
        else:
            raise "Directory not found!"
    except:
        print ("This should never print")


if __name__ == '__main__':
    """
    First we parse the arguments, which consist of:
    a) the datafile (must be a ".csv" file. If not given, default is 'experiment_data.csv')
    b) the fields in the datafile that form a combination
    c) one of the fields out of the combinations above, that can be used as the
    x-axis of the violin plot generated. The y-axis is always 'median_reward'
    d) the name of the output file generated. The file is usually generated
    in pdf format
    """
    parser = optparse.OptionParser()
    parser.add_option("-f","--file", dest="filename", help="file that is to be processed", action="store", type="string")
    parser.add_option("-c","--column", dest="column", help="file that is to be processed", action="store", type="string")
    (options, args) = parser.parse_args()
    column1,column2 = options.column.split(",")
    clone_vs_diversity(options.filename, column1, column2)
