import contextlib
import optparse
import os
import pandas as pd
import numpy


def process_files(dirname, columnlist): #, output_file):
    created_dir = ensure_dir(dirname)
    file1 = 'experiment_data.csv'
    file2 = 'diversity_data.csv'
    with working_directory(created_dir) as wd:
        print("Changed working directory to: %s" % (os.getcwd()))
        diversity_data = pd.read_csv(file2)
        summarized_diversity = diversity_data.groupby(columnlist)['ending_diversity'].median()
        summarized_diversity = summarized_diversity.to_dict()
        with open(file1, 'r') as expfile:
            data = []
            header = next(expfile).rstrip("\n")
            for line in expfile:
                recency, epsilon, reproduction_cycle,cloning_cycle, memory, player_prop, RL_prop,median, mean, variance, gini = line.rstrip('\n').split(",")
                if int(cloning_cycle) in summarized_diversity:
                    diversity_achieved = summarized_diversity[numpy.int64(cloning_cycle)]
                else:
                    diversity_achieved = 0
                result = ','.join([ recency, epsilon,reproduction_cycle,cloning_cycle, memory, player_prop, RL_prop, median, mean, variance, gini, str(diversity_achieved),'\n'])
                data.append(result)
        header = ','.join([header, 'diversity', '\n'])
        with open('unified_data.csv', 'w') as expfile:
            expfile.write(header)
            for line in data:
                expfile.write(line)



def summarize(dirname, combination, output_file):
    created_dir = ensure_dir(dirname)
    file1 = 'experiment_data.csv'
    file2 = 'diversity_data.csv'
    with working_directory(created_dir) as wd:
        print("Changed working directory to: %s" % (os.getcwd()))
        experiment_data = pd.read_csv(file1)
        #diversity_data = pd.read_csv(file2)
        summarized_experiment = experiment_data.groupby('cloning-cycle')
        print(summarized_experiment)


@contextlib.contextmanager
def working_directory(path):
    """
    changes working directory temporarily and switches back upon completion
    """
    prev_path = os.getcwd()
    os.chdir(path)
    try:
        yield
    finally:
        os.chdir(prev_path)


def ensure_dir(dirname):
    try:
        if os.path.isdir(dirname):
            return dirname
        else:
            raise "Directory not found!"
    except:
        print ("This should never print")


def find_dir(filename):
    created_dirname = os.path.basename(os.path.abspath(filename))
    created_dirname = created_dirname.split('.', 1)[0]
    created_dirname = ''.join([created_dirname, "-graphs"])
    path_to_create = os.path.dirname(os.path.abspath(filename))
    created_dirname = ''.join([path_to_create, os.sep, created_dirname])
    print("already created directory: %s" % (created_dirname))
    try:
        if os.path.isdir(created_dirname):
            return created_dirname
        else:
            raise "Directory not found!"
    except:
        print ("This should never print")


if __name__ == '__main__':
    """
    First we parse the arguments, which consist of:
    a) the directory containing the files
    b) the fields in the datafile that form a combination
    c) the name of the output file generated. The file is usually generated
    in pdf format
    """
    parser = optparse.OptionParser()
    parser.add_option("-d","--dir", dest="dirname", help="directory containing files to be processed", action="store", type="string")
    parser.add_option("-c","--columnlist", dest="columnlist", help="combinations in file to be used for join", action="store", type="string")
    #parser.add_option("-o","--file", dest="output_file", help="file that is to be written", action="store", type="string")
    (options, args) = parser.parse_args()
    columnlist = options.columnlist.split(",")
    process_files(options.dirname, columnlist) # , options.output_file)
